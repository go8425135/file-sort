package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func sliceContains(slice []string, str string) bool {

	for _, s := range slice {
		if s == str {
			return true
		}
	}

	return false
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func pathExists(path string) bool {
	_, err := os.Stat(path)

	if err == nil {
		return true
	}

	if os.IsNotExist(err) {
		return false
	}

	return false
}

func setupDirectoryTree(prefix string) {

	var dirs = []string{
		"images",
		"documents",
		"archives",
		"music",
		"misc",
		"documents/wordDocs",
		"documents/spreadsheets",
		"documents/pdfs",
		"documents/ppts",
	}

	fmt.Println("Creating directory tree...")

	for _, dir := range dirs {

		if !pathExists(prefix + dir) {

			fmt.Printf("Creating dir: %s\n", prefix+dir)
			err := os.MkdirAll(prefix+dir, 0755)
			checkErr(err)

		} else {

			fmt.Printf("*** File: %s Already Exists\n", dir)

		}

	}

}

func getFiles(dir string) []string {

	fmt.Println("Reading Directory: ", dir)

	var files = []string{}

	dirInfo, err := os.ReadDir(dir)
	checkErr(err)

	for _, file := range dirInfo {
		files = append(files, file.Name())
	}

	return files
}

func sortFiles(files []string, prefix string) {

	fmt.Println("Sorting Files...")

	var pdfs = []string{"pdf"}
	var word = []string{"doc", "docx", "odt"}
	var ppts = []string{"ppt", "odp"}
	var spreadsheets = []string{"csv", "xlsx", "ods"}

	var music = []string{"mp3", "m4a"}
	var images = []string{"png", "jpg", "jpeg", "gif", "webp", "bmp", "avif", "tiff"}
	var archives = []string{"zip", "zst", "tar", "gz"}

	for _, file := range files {

		info, err := os.Stat(prefix+file)
		checkErr(err)

		if file == "main.go" || file == "main_test.go" || info.IsDir() {
			continue
		}

		var tmpArr []string = strings.Split(file, ".")

		if len(tmpArr) == 1 {
			os.Rename(prefix+file, prefix+"misc/"+file)
			continue
		}

		var tmp string = tmpArr[len(tmpArr)-1]
		
		if sliceContains(pdfs, tmp) {
			os.Rename(prefix+file, prefix+"documents/pdfs/"+file)
		} else if sliceContains(word, tmp) {
			os.Rename(prefix+file, prefix+"documents/wordDocs/"+file)
		} else if sliceContains(ppts, tmp) {
			os.Rename(prefix+file, prefix+"documents/ppts/"+file)
		} else if sliceContains(spreadsheets, tmp) {
			os.Rename(prefix+file, prefix+"documents/spreadsheets/"+file)
		} else if sliceContains(music, tmp) {
			os.Rename(prefix+file, prefix+"music/"+file)
		} else if sliceContains(images, tmp) {
			os.Rename(prefix+file, prefix+"images/"+file)
		} else if sliceContains(archives, tmp) {
			os.Rename(prefix+file, prefix+"archives/"+file)
		} else {
			os.Rename(prefix+file, prefix+"misc/"+file)
		}
	}
}

func help() {
	fmt.Println("Usage:\n",
		"Running without options will default to your downloads folder.\n",
		"Without Options: sort-files\n",
		"With Options: sort-files [flag] <option>\n",
		"Options:\n",
		"-p <file path> - This creates the directory tree and sorts files in the given full path")

}

func main() {

	prefix, err := os.UserHomeDir()
	checkErr(err)

	prefix += "/Downloads/"

	if len(os.Args) > 1 {
		if os.Args[1] == "-p" && len(os.Args) == 3 {
			if !(os.Args[2][len(os.Args[2])-1] == '/') {
				prefix = os.Args[2] + "/"
			} else {
				prefix = os.Args[2]
			}
		} else if len(os.Args) == 2 {

			if os.Args[1] == "-h" || os.Args[1] == "--help" {
				help()
				os.Exit(0)
			} else {
				help()
				os.Exit(1)
			}

		} else {
			help()
			os.Exit(1)
		}
	}
	fmt.Println("Running in direcory of: ", prefix)
	setupDirectoryTree(prefix)
	sortFiles(getFiles(prefix), prefix)
}
