package main

import (
	"log"
	"os"
	"testing"
	// "fmt"
)

func TestPathExists(t *testing.T) {
	os.RemoveAll("test")

	if pathExists("test") {
		t.Errorf("'test' dir should return false")
	}

	err := os.Mkdir("test2", 0755)
	if err != nil {
		log.Fatal(err)
	}

	if !pathExists("test2") {
		t.Errorf("'test2' dir should return true")
	}

	os.RemoveAll("test2")
}

var dirs = []string{
	"images",
	"documents",
	"archives",
	"music",
	"misc",
	"documents/wordDocs",
	"documents/spreadsheets",
	"documents/pdfs",
	"documents/ppts",
}

func TestSetupDirectoryTree(t *testing.T) {

	setupDirectoryTree("./")

	for _, dir := range dirs {
		if !pathExists(dir) {
			t.Errorf("'%s' wasn't created", dir)
		}
	}

	for _, dir := range dirs {
		os.RemoveAll(dir)
	}

}

func TestGetFiles(t *testing.T) {
	var files = getFiles("./")
	var testFiles = [2]string{"main.go", "main_test.go"}

	for i, file := range testFiles {
		if file != files[i] {
			t.Errorf("File: %s not found", file)
		}
	}
}

func TestSliceContains(t *testing.T) {
	trueStr := "test"
	falseStr := "no"
	var strArr = []string{"test", "help", "test1", "me", "please", "nope"}

	if !sliceContains(strArr, trueStr) {
		t.Error("Returned false on present string.")
	}

	if sliceContains(strArr, falseStr) {
		t.Error("Returned true on a not present string.")
	}
}

func TestSortFiles(t *testing.T) {

	var fileTypes = []string{
		"test.txt",
		"test.pdf",
		"test.doc",
		"test.docx",
		"test.odt",
		"test.ppt",
		"test.odp",
		"test.csv",
		"test.xlsx",
		"test.ods",
		"test.mp3",
		"test.m4a",
		"test.png",
		"test.jpg",
		"test.jpeg",
		"test.gif",
		"test.webp",
		"test.bmp",
		"test.avif",
		"test.tiff",
		"test.zip",
		"test.tar",
		"test.gz",
		"test.zst",
	}

	for _, f := range fileTypes {
		_, err := os.Create(f)
		checkErr(err)
	}

	setupDirectoryTree("./")
	var files []string = getFiles("./")
	sortFiles(files, "./")

	var correctFilePaths = []string{
		"misc/test.txt",
		"documents/pdfs/test.pdf",
		"documents/wordDocs/test.doc",
		"documents/wordDocs/test.docx",
		"documents/wordDocs/test.odt",
		"documents/ppts/test.ppt",
		"documents/ppts/test.odp",
		"documents/spreadsheets/test.csv",
		"documents/spreadsheets/test.xlsx",
		"documents/spreadsheets/test.ods",
		"music/test.mp3",
		"music/test.m4a",
		"images/test.png",
		"images/test.jpg",
		"images/test.jpeg",
		"images/test.gif",
		"images/test.webp",
		"images/test.bmp",
		"images/test.avif",
		"images/test.tiff",
		"archives/test.zip",
		"archives/test.tar",
		"archives/test.gz",
		"archives/test.zst",
	}

	for _, f := range correctFilePaths {
		_, err := os.Stat(f)
		if err != nil {
			t.Errorf("File: %s Did not sort properly", f)
		}
	}

	for _, dir := range dirs {
		os.RemoveAll(dir)
	}
}
