# File Sort

## Purpose

This program was created to help me learn the Go programming language and keep
my downloads folder in check. It will create the following directory structure:

```
.
├── archives
├── documents
│   ├── pdfs
│   ├── ppts
│   ├── spreadsheets
│   └── wordDocs
├── images
├── misc
└── music
```

## Use

Running without options will default to your downloads folder.  
Without Options: sort-files  
With Options: sort-files [flag] <option>  
  
Options:  
-p <file path> - This creates the directory tree and sorts files in the given full path  

Running repeatedly on the same directory will place any new files into the previously
created directories.

## Installing

1. Install the Go programming language.
1. Clone this project
1. cd into "file-sort"
1. cd into "sort-files"
1. Run `go install`

If this is not working or for an alternate install, follow steps 1 to 4. Then
run `go build`. An executable called sort-files will be created. Place this wherever
binaries are stored on your system.
